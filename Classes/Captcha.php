<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 11/1/2016
 * Time: 9:55 AM
 */
class Captcha {

    public $bg_path;
    public $font_path;
    public $diff_color;

    public $prom_array;

    function __construct($config = null)
    {
        // Check for GD library
        if( !function_exists('gd_info') ) {
            throw new Exception('Required GD library is missing');
        }

        $bg_path = dirname(__FILE__) . '/../backgrounds/';
        $font_path = dirname(__FILE__) . '/../fonts/';

        $diff_color = dechex(rand(10,12303291));
        // Default values
        $this->prom_array = array(
            'code' => '',
            'min_length' => 4,
            'max_length' => 5,
            'backgrounds' => array(
                $bg_path . '45-degree-fabric.png',
                $bg_path . 'cloth-alike.png',
                $bg_path . 'grey-sandbag.png',
                $bg_path . 'kinda-jean.png',
                $bg_path . 'polyester-lite.png',
                $bg_path . 'stitched-wool.png',
                $bg_path . 'white-carbon.png',
                $bg_path . 'white-wave.png'
            ),
            'fonts' => array(
                $font_path . 'times_new_yorker.ttf'
            ),
            'characters' => 'ABCDEFGHJKLMNPRSTUVWXYZabcdefghjkmnprstuvwxyz1234567890',
            'min_font_size' => 15,
            'max_font_size' => 30,
            'color' => "#" . $diff_color,
            'angle_min' => 10,
            'angle_max' => 20,
            'shadow' => true,
            'shadow_color' => '#fff',
            'shadow_offset_x' => -1,
            'shadow_offset_y' => 1
        );

        // Overwrite defaults with custom config values
        if( is_array($config) ) {
            foreach( $config as $key => $value ) $this->prom_array[$key] = $value;
        }

        // Restrict certain values
        if( $this->prom_array['min_length'] < 1 ) $this->prom_array['min_length'] = 1;
        if( $this->prom_array['angle_min'] < 0 ) $this->prom_array['angle_min'] = 0;
        if( $this->prom_array['angle_max'] > 10 ) $this->prom_array['angle_max'] = 10;
        if( $this->prom_array['angle_max'] < $this->prom_array['angle_min'] ) $this->prom_array['angle_max'] = $this->prom_array['angle_min'];
        if( $this->prom_array['min_font_size'] < 10 ) $this->prom_array['min_font_size'] = 10;
        if( $this->prom_array['max_font_size'] < $this->prom_array['min_font_size'] ) $this->prom_array['max_font_size'] = $this->prom_array['min_font_size'];

        // Generate CAPTCHA code if not set by user
        if( empty($this->prom_array['code']) ) {
            $this->prom_array['code'] = '';
            $length = rand($this->prom_array['min_length'], $this->prom_array['max_length']);
            while( strlen($this->prom_array['code']) < $length ) {
                $this->prom_array['code'] .= substr($this->prom_array['characters'], rand() % (strlen($this->prom_array['characters'])), 1);
            }
        }


        $_SESSION['captcha_code'] = $this->prom_array['code'];

    }

    private function hex2rgb($hex_str, $return_string = false, $separator = ',') {
        $hex_str = preg_replace("/[^0-9A-Fa-f]/", '', $hex_str); // Gets a proper hex string
        $rgb_array = array();
        if( strlen($hex_str) == 6 ) {
            $color_val = hexdec($hex_str);
            $rgb_array['r'] = 0xFF & ($color_val >> 0x10);
            $rgb_array['g'] = 0xFF & ($color_val >> 0x8);
            $rgb_array['b'] = 0xFF & $color_val;
        } elseif( strlen($hex_str) == 3 ) {
            $rgb_array['r'] = hexdec(str_repeat(substr($hex_str, 0, 1), 2));
            $rgb_array['g'] = hexdec(str_repeat(substr($hex_str, 1, 1), 2));
            $rgb_array['b'] = hexdec(str_repeat(substr($hex_str, 2, 1), 2));
        } else {
            return false;
        }
        return $return_string ? implode($separator, $rgb_array) : $rgb_array;
    }


    function getImage() {


        $background = $this->prom_array['backgrounds'][rand(0, count($this->prom_array['backgrounds']) -1)];
        list($bg_width, $bg_height, $bg_type, $bg_attr) = getimagesize($background);

        $captcha = imagecreatefrompng($background);

        $color = $this->hex2rgb($this->prom_array['color']);
        $color = imagecolorallocate($captcha, $color['r'], $color['g'], $color['b']);

        // Determine text angle
        $angle = rand( $this->prom_array['angle_min'], $this->prom_array['angle_max'] ) * (rand(0, 1) == 1 ? -1 : 1);

        // Select font randomly
        $font = $this->prom_array['fonts'][rand(0, count($this->prom_array['fonts']) - 1)];

        // Verify font file exists
        if( !file_exists($font) ) throw new Exception('Font file not found: ' . $font);

        //Set the font size.
        $font_size = rand($this->prom_array['min_font_size'], $this->prom_array['max_font_size']);
        $text_box_size = imagettfbbox($font_size, $angle, $font, $this->prom_array['code']);

        // Determine text position
        $box_width = abs($text_box_size[6] - $text_box_size[2]);
        $box_height = abs($text_box_size[5] - $text_box_size[1]);
        $text_pos_x_min = 0;
        $text_pos_x_max = ($bg_width) - ($box_width);
        $text_pos_x = rand($text_pos_x_min, $text_pos_x_max);
        $text_pos_y_min = $box_height;
        $text_pos_y_max = ($bg_height) - ($box_height / 2);
        if ($text_pos_y_min > $text_pos_y_max) {
            $temp_text_pos_y = $text_pos_y_min;
            $text_pos_y_min = $text_pos_y_max;
            $text_pos_y_max = $temp_text_pos_y;
        }
    $text_pos_y = rand($text_pos_y_min, $text_pos_y_max);

    // Draw shadow
    if( $this->prom_array['shadow'] ){
        $shadow_color = $this->hex2rgb($this->prom_array['shadow_color']);
        $shadow_color = imagecolorallocate($captcha, $shadow_color['r'], $shadow_color['g'], $shadow_color['b']);
        imagettftext($captcha, $font_size, $angle, $text_pos_x + $this->prom_array['shadow_offset_x'], $text_pos_y + $this->prom_array['shadow_offset_y'], $shadow_color, $font, $this->prom_array['code']);
    }

    // Draw text
    imagettftext($captcha, $font_size, $angle, $text_pos_x, $text_pos_y, $color, $font, $this->prom_array['code']);



        //ob_start();
        imagepng($captcha);
        //$image = base64_encode(ob_get_contents());
        //ob_end_clean();
        //return $image;
    }




}