<?php

class DB {
    static public $connection;

    static public function init()   {
        $con = 'mysql:host=' . DATABASE_HOST . ';dbname=' . DATABASE_DATABASE.";charset=utf8;port=".DATABASE_PORT ;   //this is how we call define down in the code
        try {
            self::$connection  = new PDO($con, DATABASE_USER, DATABASE_PASSWORD);
            self::$connection ->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC); //fetch() returns only sociative arrays
        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
    }
}