function numberRestrict(data)   {
    return /[0-9]/.test(data);
}

//Check if data contains any other char beside latin letter or number
function latinRestrict(data)   {
    return !/[^a-z0-9]/i.test(data);
}

function cyrillicRestrict(data)   {
    return /^[а-я А-ЯЁё]+$/.test(data);
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}