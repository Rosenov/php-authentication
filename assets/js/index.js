$(".section-container").css({"margin-top" : (($(window).height() - $(".section-container").height()) / 2) - $('header').height() + "px"});

$(".expired-pass-reset-query").css({"margin-top" : (($(window).height() - $(".expired-pass-reset-query").height()) / 2) - $('header').height() + "px"});


if($("body").hasClass("login")) {
    $("#user-name").keyup(function(event){
        if(event.keyCode == 13){
            $('.login-btn-container > input').click();
        }
    });
    $("#user-pass").keyup(function(event){
        if(event.keyCode == 13){
            $('.login-btn-container > input').click();
        }
    });
}else if($("body").hasClass("register")) {
    $("#user-name").keyup(function(event){
        if(event.keyCode == 13){
            $('.register-btn-container > input').click();
        }
    });
    $("#user-first-pass").keyup(function(event){
        if(event.keyCode == 13){
            $('.register-btn-container > input').click();
        }
    });
    $("#user-second-pass").keyup(function(event){
        if(event.keyCode == 13){
            $('.register-btn-container > input').click();
        }
    });
    $("#user-email").keyup(function(event){
        if(event.keyCode == 13){
            $('.register-btn-container > input').click();
        }
    });
}

/*======================================AJAXES=========================================*/
//LOGIN
$('.login-btn-container > input').click(function() {
    if($("#user-name").val() == "" || $("#user-pass").val() == "")  {
        alert("One of the fields is empty!");
    }else if(latinRestrict($("#user-name").val()) == false) {
        alert('Please type only in latin!');
    }else if(latinRestrict($("#user-pass").val()) == false) {
        alert('Please type only in latin!');
    }else if($("#user-name").val().length > 30 || $("#user-pass").val().length > 30)  {
        alert('Your fields can contain up to 30 characters!');
    }else if($("#user-name").val().length < 6 || $("#user-pass").val().length < 6)  {
        alert('Your fields cannot contain less than 6 characters!');
    }else{
        $.ajax({
            url: HOME_URL + "index.php",
            data: {
                "action" : "authenticate-user",
                'user-name' : $("#user-name").val(),
                'user-pass' : $("#user-pass").val()
            },  //object
            type: 'POST',
            dataType: 'json',
            async: true,
            success: function (response) {
                //showDialog('dialog', response, "register_dialog", "reg_receipt_again");
                if(response.success) {
                    alert(response.success);
                    window.location.reload();
                }else if(response.error)    {
                    alert(response.error);
                }
            }
        })
    }
});

//REGISTER
$('.register-btn-container > input').click(function() {
    if($("#user-name").val() == "" || $("#user-first-pass").val() == "" || $("#user-second-pass").val() == "" || $("#user-email").val() == "")  {
        alert('Please make sure all fields are not empty!');
    }else if(latinRestrict($("#user-name").val()) == false) {
        alert('Please type only in latin!');
    }else if(latinRestrict($("#user-first-pass").val()) == false) {
        alert('Please type only in latin!');
    }else if(latinRestrict($("#user-second-pass").val()) == false) {
        alert('Please type only in latin!');
    }else if($("#user-first-pass").val() != $("#user-second-pass").val()) {
        alert('Please make sure both passwords match!');
    }else if(validateEmail($("#user-email").val()) == false) {
        alert('Please make sure you entered valid email!');
    }else if($('#register-rules-checkbox:checked').length != true){
        alert('Please make sure you have checked Website rules!');
    }else if($("#user-name").val().length > 30 || $("#user-first-pass").val().length > 30 || $("#user-second-pass").val().length > 30)  {
        alert('Your fields can contain up to 30 characters!');
    }else if($("#user-name").val().length < 6 || $("#user-first-pass").val().length < 6 || $("#user-second-pass").val().length < 6)  {
        alert('Your fields cannot contain less than 6 characters!');
    }else if($("#user-email").val().length > 50)  {
        alert('Your email field can contain up to 50 characters!');
    }else{
        $.ajax({
            url: HOME_URL + "index.php",
            data: {
                "action" : "register-user",
                'user-name' : $("#user-name").val(),
                'user-first-pass' : $("#user-first-pass").val(),
                'user-second-pass' : $("#user-second-pass").val(),
                'user-email' : $("#user-email").val(),
                'website-rules' : $('#register-rules-checkbox:checked').length
            },  //object
            type: 'POST',
            dataType: 'json',
            async: true,
            success: function (response) {
                //showDialog('dialog', response, "register_dialog", "reg_receipt_again");
                if(response.success)    {
                    alert(response.success);
                    window.location = HOME_URL;
                }else if(response.error)    {
                    alert(response.error);
                }
            }
        })
    }
});

//FORGOTTEN PASSWORD
$('.forgotten-password-send-btn').click(function() {
    if($("#forgotten-sent-email").val() == "")  {
        alert("Please make sure you entered email!")
    }else if(validateEmail($("#forgotten-sent-email").val()) == false) {
        alert('Please make sure you entered valid email!');
    }else if($("#forgotten-sent-email").val().length > 50)  {
        alert('Your email field can contain up to 50 characters!');
    }else {
        $.ajax({
            url: HOME_URL + "index.php",
            data: {
                "action" : "forgotten-password",
                'forgotten-sent-email' : $('#forgotten-sent-email').val()
            },  //object
            type: 'POST',
            dataType: 'json',
            async: true,
            success: function (response) {
                //showDialog('dialog', response, "register_dialog", "reg_receipt_again");
                if(response.success)    {
                    alert(response.success);
                    window.location = HOME_URL;
                }else if(response.error)    {
                    alert(response.error);
                }
            }
        })
    }
});

//RESETTING PASSWORD
$('.resetting-new-password-send-btn').click(function() {
    if($("#resetting-first-password").val() == "" || $("#resetting-second-password").val() == "")   {
        alert('Please make sure all fields are not empty!');
    }else if(latinRestrict($("#resetting-first-password").val()) == false) {
        alert('Please type only in latin without special symbols!');
    }else if(latinRestrict($("#resetting-second-password").val()) == false) {
        alert('Please type only in latin without special symbols!');
    }else if($("#resetting-first-password").val() != $("#resetting-second-password").val()) {
        alert('Please make sure both passwords match!');
    }else if($("#resetting-second-password").val().length > 30 || $("#resetting-second-password").val().length > 30){
        alert('Your fields can contain up to 30 characters!');
    }else if($("#resetting-second-password").val().length < 6 || $("#resetting-second-password").val().length > 6){
        alert('Your fields cannot contain less than 6 characters!');
    }else {
        $.ajax({
            url: HOME_URL + "index.php",
            data: {
                "action" : "resetting-password",
                'user-first-pass' : $("#resetting-first-password").val(),
                'user-second-pass' : $("#resetting-second-password").val(),
                'user-id' : $('.resetting-new-password-send-btn').data("user_id")

            },  //object
            type: 'POST',
            dataType: 'json',
            async: true,
            success: function (response) {
                //showDialog('dialog', response, "register_dialog", "reg_receipt_again");
                if(response.success)    {
                    alert(response.success);
                    window.location = HOME_URL;
                }else if(response.error)    {
                    alert(response.error);
                }
            }
        })
    }
});

//LOG OUT
$('.header-btns-container > .logout').click(function() {
    event.preventDefault();
    $.ajax({
        url: HOME_URL + "index.php",
        data: {
            'action' : "logout"
        },  //object
        type: 'POST',
        dataType: 'json',
        async: true,
        success: function (response) {
            if(response.success)    {
                window.location.reload();
            }else if(response.error)    {
                //sweetAlert('Error', response.error, 'error');
            }
        }
    })
});