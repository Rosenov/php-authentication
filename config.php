<?php

require_once 'functions.php';

session_start();
define("HOME_URL", "http://localhost/php-authentication/");
define("HOME_DIRECTORY", __DIR__ . "/");
define('DATABASE_HOST','localhost');
define('DATABASE_USER','root');
define('DATABASE_PASSWORD','');
define('DATABASE_DATABASE', 'php-authentication');
define('DATABASE_PORT','3306');

function __autoload($class_name){
    if (file_exists(HOME_DIRECTORY . "Classes/" . $class_name . '.php')) {
        require_once HOME_DIRECTORY . "Classes/" . $class_name . '.php';
    }else if (file_exists(HOME_DIRECTORY . "controllers/" . $class_name . '.php')) {
        require_once HOME_DIRECTORY . "controllers/" . $class_name . '.php';
    }
}

DB::init();
?>
