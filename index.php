<?php
    require_once 'config.php';
    Router::onPageLoad();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <script src="assets/libs/jquery-3.1.1.min.js"></script>
</head>
<body class="<?php
    if(isset($_GET['page']))    {
        echo $_GET['page'];
    }
?>">
    <div>
        <header>
            <h1>PHP Authentication</h1>
            <div class="header-btns-container">
                <?php
                    echo "<a href=" . HOME_URL . ">Rules</a>";
                    if(!isset($_SESSION['logged_user_id']))    {
                        echo "<a href=" . HOME_URL . 'login' . ">Login</a>";
                        echo "<a href=" . HOME_URL . 'register' . ">Register</a>";
                    }else if(isset($_SESSION['logged_user_id']))   {
                        echo "<a href='#' class='logout'>Logout</a>";
                    }
                ?>
            </div>
        </header>
        <section>
            <?php
                if(!isset($_SESSION['logged_user_id']))    {
                    require_once 'templates/not-logged.php';
                }else if(isset($_SESSION['logged_user_id'])) {
                    require_once 'templates/dashboard.php';
                }
                if(empty($_GET)){
                    require_once 'templates/authentication-rules.php';
                }
                Router::campingForPassReset();
            ?>
        </section>
        <footer></footer>
    </div>
<script type="text/javascript">
    var HOME_URL = "<?php echo HOME_URL; ?>";
</script>
<script src="assets/js/basic.js"></script>
<script src="assets/js/index.js"></script>
</body>
</html>
