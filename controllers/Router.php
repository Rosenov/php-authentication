<?php

class Router {
    static public function onPageLoad()   {
        if(isset($_POST['action'])) {
            if($_POST['action'] == 'register-user') {
                (new User())->registerUser();
            }else if($_POST['action'] == 'authenticate-user') {
                (new User())->authenticateUser();
            }else if($_POST['action'] == 'forgotten-password')  {
                (new User())->forgottenPassword();
            }else if($_POST['action'] == 'resetting-password')  {
                (new User())->resettingPassword();
            }else if($_POST['action'] == 'logout')  {
                Login::logOut();
            }
        }
    }

    static public function campingForPassReset()    {
        if(!empty($_GET['forgotten_pass'])) {
            (new User())->authenticatePassReset();
        }
    }
}