<?php

class User {
    function registerUser() {
        $data = array();
        if(empty($_POST['user-name']) && empty($_POST['user-first-pass']) && empty($_POST['user-second-pass']) && empty($_POST['user-email']) && $_POST['website-rules'] == 0)  {
            $data['error'] = 'Please make sure you have entered all fields!';
        }else if(checkIfLettersAndNums($_POST['user-name']) || checkIfLettersAndNums($_POST['user-first-pass']) || checkIfLettersAndNums($_POST['user-second-pass']))  {
            $data['error'] = 'Please make sure you have entered all fields in latin!';
        }else if(!filter_var($_POST['user-email'], FILTER_VALIDATE_EMAIL)) {
            $data['error'] = 'Please make sure you entered valid email!';
        }else if(strlen($_POST['user-name']) > 30 || strlen($_POST['user-first-pass']) > 30 || strlen($_POST['user-second-pass']) > 30)   {
            $data['error'] = 'Your fields can contain up to 30 characters!';
        }else if(strlen($_POST['user-name']) < 6 || strlen($_POST['user-first-pass']) < 6 || strlen($_POST['user-second-pass']) < 6)   {
            $data['error'] = 'Your fields cannot contain less than 6 characters!';
        }else if(strlen($_POST['user-email']) > 50) {
            $data['error'] = 'Your email field can contain up to 50 characters!';
        }else{
            $check_email_query = DB::$connection->query("SELECT username, email FROM users")->fetchAll();
            $list_with_usernames = array();
            $list_with_emails = array();
            foreach ($check_email_query as $check_single_email) {
                array_push($list_with_usernames, $check_single_email['username']);
                array_push($list_with_emails, $check_single_email['email']);
            }

            if(in_array($_POST['user-email'], $list_with_emails))  {
                $data['error'] = 'This email is already registered, please use another one!';
            }else if(in_array($_POST['user-name'], $list_with_usernames))   {
                $data['error'] = 'This username is already registered, please use another one!';
            }else{
                for ($i = -1; $i <= 16; $i++) {
                    $bytes = openssl_random_pseudo_bytes($i, $cstrong);
                    $salt = bin2hex($bytes);
                }
                $password_hash = hash_pbkdf2("sha256", strip_tags($_POST['user-first-pass']), $salt, 1000, 32);

                $reg_user = DB::$connection->exec("INSERT INTO users (`username`, `password`, `salt`, `email`, `created_at`) VALUES ('" . strip_tags($_POST['user-name']) . "', '" . $password_hash . "', '" . $salt . "', '" . strip_tags($_POST['user-email']) . "', '" . date('Y-m-d H:i:s') . "')");
                if(!$reg_user)  {
                    $data['error'] = 'User not registered!';
                }else {
                    $data['success'] = 'Account has been registered successfully!';
                }
            }
        }
        echo json_encode($data);
        die();
    }

    function authenticateUser() {
        $data = array();
        if(empty($_POST['user-name']) || empty($_POST['user-pass']))  {
            $data['error'] = "One of the fields is empty!";
        }else if(checkIfLettersAndNums($_POST['user-name']) || checkIfLettersAndNums($_POST['user-pass']))  {
            $data['error'] = 'Please make sure you have entered all fields in latin!';
        }else if(strlen($_POST['user-name']) > 30 || strlen($_POST['user-pass']) > 30)   {
            $data['error'] = 'Your fields can contain up to 30 characters!';
        }else if(strlen($_POST['user-name']) < 6 || strlen($_POST['user-pass']) < 6)   {
            $data['error'] = 'Your fields cannot contain less than 6 characters!';
        }else {
            $get_user = DB::$connection->query("SELECT u.`username`, u.`id`, u.`password`, u.`salt` FROM users as u WHERE u.`username`='" . $_POST['user-name'] . "'")->fetch();
            $password_hash = hash_pbkdf2("sha256", $_POST['user-pass'], $get_user['salt'], 1000, 32);
            if($password_hash == $get_user['password']) {
                $_SESSION['logged_user_id'] = $get_user['id'];
                $_SESSION['logged_user_username'] = $get_user['username'];
                $data['success'] = 'Authentication successful!';
            }else {
                $data['error'] = "Wrong username or password!";
            }
        }
        echo json_encode($data);
        die();
    }

    function forgottenPassword()    {
        $data = array();
        if(empty($_POST['forgotten-sent-email']))   {
            $data['error'] = 'Please make sure you have entered email!';
        }else if(!filter_var($_POST['forgotten-sent-email'], FILTER_VALIDATE_EMAIL)) {
            $data['error'] = 'Please make sure you entered valid email!';
        }else if(strlen($_POST['forgotten-sent-email']) > 50) {
            $data['error'] = 'Your email field can contain up to 50 characters!';
        }else {
            $check_email = DB::$connection->query("SELECT * FROM `users` WHERE `email` = '" . $_POST['forgotten-sent-email'] . "'")->fetch();
            if($check_email)    {
                $time_last_forgotten_password = strtotime($check_email['forgotten_pass_req_timestamp']);
                if(strtotime(date('Y-m-d H:i:s', strtotime('-15 minute'))) < $time_last_forgotten_password) {
                    $data['error'] = "You've already sent password reset request. Please try again in 15 minutes!";
                }else {
                    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $charactersLength = strlen($characters);
                    $randomString = '';
                    for ($i = 0; $i < 50; $i++) {
                        $randomString .= $characters[rand(0, $charactersLength - 1)];
                    }
                    $update_forgotten_pass_id = DB::$connection->exec("UPDATE users SET `forgotten_pass_id` = '" . $randomString . "', `forgotten_pass_req_timestamp` = '" . date('Y-m-d H:i:s') . "' WHERE `id` = " . $check_email['id']);

                    if($update_forgotten_pass_id)   {
                        $message = 'For changing password please click: <a href="' . HOME_URL . '?forgotten_pass=' . $randomString . '" target="_blank">HERE</a>';
                        sendMail($_POST['forgotten-sent-email'], "Change password request", $message);
                        $data['success'] = 'Mail have been sent to your post, please check it!';
                    }else {
                        $data['error'] = 'Something went wrong, please try again later!';
                    }
                }
            }else {
                $data['error'] = 'No user exist with this email!';
            }
        }
        echo json_encode($data);
        die();
    }

    function resettingPassword()    {
        $data = array();
        if(empty($_POST['user-first-pass']) || empty($_POST['user-second-pass']) || empty($_POST['user-id']))  {
            $data['error'] = 'Please fill both passwords!';
        }else if(checkIfLettersAndNums($_POST['user-first-pass']) || checkIfLettersAndNums($_POST['user-second-pass']))   {
            $data['error'] = 'Please fill both passwords only in latin without any special symbols!';
        }else if(strlen($_POST['user-first-pass']) > 30 || strlen($_POST['user-second-pass']) > 30)   {
            $data['error'] = 'Your fields can contain up to 30 characters!';
        }else if(strlen($_POST['user-first-pass']) < 6 || strlen($_POST['user-second-pass']) < 6)   {
            $data['error'] = 'Your fields cannot contain less than 6 characters!';
        }else if($_POST['user-first-pass'] != $_POST['user-second-pass'])  {
            $data['error'] = 'Please make sure both passwords match!';
        }else {
            for ($i = -1; $i <= 16; $i++) {
                $bytes = openssl_random_pseudo_bytes($i, $cstrong);
                $salt = bin2hex($bytes);
            }
            $password_hash = hash_pbkdf2("sha256", strip_tags($_POST['user-first-pass']), $salt, 1000, 32);
            $resetting_password_query = DB::$connection->exec("UPDATE users SET `password` = '" . $password_hash . "', `salt` = '" . $salt . "' WHERE `id` = " . $_POST['user-id']);
            if($resetting_password_query)   {
                $data['success'] = 'Password changed successfully!';
            }else {
                $data['error'] = 'Password cannot be changed at the moment!';
            }
        }
        echo json_encode($data);
        die();
    }

    function authenticatePassReset()    {
        $check_if_user_has_pass_id = DB::$connection->query("SELECT * FROM users WHERE `forgotten_pass_id` = '" . $_GET['forgotten_pass'] . "'")->fetch();
        if($_GET['forgotten_pass'] == $check_if_user_has_pass_id['forgotten_pass_id'])    {
            if($check_if_user_has_pass_id)  {
                $time_requested_pass_res = $check_if_user_has_pass_id['forgotten_pass_req_timestamp'];
                if(strtotime($time_requested_pass_res) > strtotime(date('Y-m-d H:i:s', strtotime('-15 minute'))))  {
                    $user_id = $check_if_user_has_pass_id['id'];
                    require_once 'templates/resetting-new-password.php';
                }else {
                    echo "<div class='expired-pass-reset-query'>We apologize your password reset request has expired, please generate a new one!</div>";
                }
            }else {
                echo "<div class='expired-pass-reset-query'>We apologize your password reset request has expired, please generate a new one!</div>";
            }
        }
    }
}