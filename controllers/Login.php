<?php

class Login {
    static public function logOut()    {
        $data = array();
        unset($_SESSION);
        session_destroy();
        if(empty($_SESSION))    {
            $data['success'] = "Log out successful!";
        }
        echo json_encode($data);
        die();
    }

    static public function isLogged()   {
        if(isset($_SESSION['logged_user_id']))  {
            return true;
        }else {
            return false;
        }
    }
}