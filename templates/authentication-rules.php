<div class="authentication-rules-title">Authentication rules:</div>
<ul>
    <li class="authentication-rule">You must use only latin letters</li>
    <li class="authentication-rule">You cannot use any special symbols</li>
    <li class="authentication-rule">Your username and password inputs must be with min length 6</li>
    <li class="authentication-rule">Your username and password inputs must be with max length 30</li>
    <li class="authentication-rule">You must use valid emails</li>
    <li class="authentication-rule">You must type 2 identical passwords for register</li>
    <li class="authentication-rule">You must check Website rules for register</li>
    <li class="authentication-rule">You must use unique username and email for register</li>
    <li class="authentication-rule">After sending request for password reset you must wait 15min for next password reset</li>
    <li class="authentication-rule">After resetting your password you don't have access anymore to the pass reset link which you received in your email, you must 15min and send new request</li>
</ul>