<div class="forgotten-password-container section-container">
    <div class="forgotten-password-input-container">
        <div class="page-label">Enter email.</div>
        <input type="text" id="forgotten-sent-email" maxlength="50"/>
    </div>
    <div class="forgotten-password-btn-container">
        <input type="button" class="forgotten-password-send-btn blue-btn" value="Send"/>
    </div>
</div>