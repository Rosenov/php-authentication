<div class="resetting-new-password-container section-container">
    <div class="resetting-new-password-input-container">
        <div class="page-label">New password</div>
        <input type="password" id="resetting-first-password" maxlength="30"/>
    </div>
    <div class="resetting-new-password-input-container">
        <div class="page-label">Confirm new password</div>
        <input type="password" id="resetting-second-password" maxlength="30"/>
    </div>
    <div class="resetting-new-password-btn-container">
        <input type="button" class="resetting-new-password-send-btn blue-btn" data-user_id="<?php echo $user_id; ?>" value="Send"/>
    </div>
</div>