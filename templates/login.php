<div class="login-container section-container">
    <div class="login-input-container">
        <div class="page-label">Username</div>
        <input type="text" id="user-name" maxlength="30"/>
    </div>
    <div class="login-input-container">
        <div class="page-label">Password</div>
        <input type="password" id="user-pass" maxlength="30"/>
    </div>
    <div class="login-btn-container">
        <input type="button" class="blue-btn" value="Log in"/>
    </div>
    <div class="login-forgot-pass-container">
        <a href="<?php echo HOME_URL . 'forgot-password'; ?>">Forgot password?</a>
    </div>
</div>