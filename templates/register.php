<div class="register-container section-container">
    <div class="register-input-container">
        <div class="page-label">Username</div>
        <input type="text" id="user-name" maxlength="30"/>
    </div>
    <div class="register-input-container">
        <div class="page-label">Password</div>
        <input type="password" id="user-first-pass" maxlength="30"/>
    </div>
    <div class="register-input-container">
        <div class="page-label">Re-enter Password</div>
        <input type="password" id="user-second-pass" maxlength="30"/>
    </div>
    <div class="register-input-container">
        <div class="page-label">Email</div>
        <input type="text" id="user-email" maxlength="50"/>
    </div>
    <div class="register-input-container">
        <div class="page-label inline-block">Website rules</div>
        <input type="checkbox" id="register-rules-checkbox" />
    </div>
    <div class="register-btn-container">
        <input type="button" class="blue-btn" value="Register"/>
    </div>
</div>
